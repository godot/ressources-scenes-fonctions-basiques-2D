extends RigidBody2D

const gravity = Vector2(0, 98) # The default gravity, you can use your own.
const SPEED = 200

func _integrate_forces(state):
	var dt = state.get_step()
	var velocity = state.get_linear_velocity()
	var some_normal_vector = Vector2(0, 1) 

	var direction = some_normal_vector * gravity.length()
	
	state.set_linear_velocity(velocity + direction * dt)

func _ready():
	set_use_custom_integrator(true)
	set_process_input(true)
	set_physics_process(true)
	
func _physics_process(delta):
	pass