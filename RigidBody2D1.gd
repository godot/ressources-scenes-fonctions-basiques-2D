extends RigidBody2D

const gravity = Vector2(0, 980) # The default gravity, you can use your own.
const SPEED = 200


func _ready():
	pass
	
func _physics_process(delta):
	if(Input.is_action_pressed("ui_left") or Input.is_action_pressed("ui_right") or Input.is_action_pressed("ui_up") or Input.is_action_pressed("ui_down") ):
		if Input.is_action_pressed("ui_left"):
			set_applied_force(Vector2(-200,0))
		elif Input.is_action_pressed("ui_right"):
			set_applied_force(Vector2(200,0))
			#set_linear_velocity(Vector2(+100,0))
		elif Input.is_action_pressed("ui_up"):
			set_applied_force(Vector2(0,-200))
		elif Input.is_action_pressed("ui_down"):
			set_applied_force(Vector2(0,200))
	else:
		set_applied_force(Vector2(0,0))
